﻿using System.Collections.Generic;
using UnityEngine;

public enum CharacterPositionStates
{
    ON_GROUND,
    ON_AIR
}

public enum CharacterMovingStates
{
    IS_STANDING,
    IS_MOVING
}

public enum CharacterStatusStates
{
    ON_QUITE,
    ON_PURSUIT,
    ON_WASBITTED
}

[System.Serializable]
public class Stats
{
    public float movementSpeed;
    public float visibleZone;
    public float agressionZone;
    public float angleDamp;
    public float occupiedAreaX;
    public float occupiedAreaZ;
    public float fieldOfViewAngle;

    public Stats(int Level, string Name) // test level
    {
        if (Name == "Character")
        {
            if (Level == 0)
            {
                movementSpeed = 250;
                visibleZone = 6;
                angleDamp = 80;
                occupiedAreaX = occupiedAreaZ = 2;
            }
            if (Level == 1)
            {
                movementSpeed = 250;
                visibleZone = 6;
                angleDamp = 80;
                occupiedAreaX = occupiedAreaZ = 2;
            }
            if (Level == 2)
            {
                movementSpeed = 280;
                visibleZone = 4;
                angleDamp = 80;
                occupiedAreaX = occupiedAreaZ = 2;
            }
            if (Level == 3)
            {
                movementSpeed = 350;
                visibleZone = 2;
                angleDamp = 80;
                occupiedAreaX = occupiedAreaZ = 2;
            }
        }
        if (Name == "Enemy")
        {
            if (Level == 0)
            {
                movementSpeed = 180;
                visibleZone = 7;
                angleDamp = 82;
                occupiedAreaX = occupiedAreaZ = 2;
                fieldOfViewAngle = 360;
                agressionZone = 10;

            }
            if (Level == 1)
            {
                movementSpeed = 180;
                visibleZone = 7;
                angleDamp = 82;
                occupiedAreaX = occupiedAreaZ = 2;
                fieldOfViewAngle = 360;
                agressionZone = 10;

            }
            if (Level == 2)
            {
                movementSpeed = 200;
                visibleZone = 7;
                angleDamp = 82;
                occupiedAreaX = occupiedAreaZ = 2;
                fieldOfViewAngle = 360;
                agressionZone = 10;
            }
            if (Level == 3)
            {
                movementSpeed = 300;
                visibleZone = 7;
                angleDamp = 82;
                occupiedAreaX = occupiedAreaZ = 2;
                fieldOfViewAngle = 360;
                agressionZone = 10;
            }
        }
    }
    
    public float MovementSpeed
    {
        get
        {
            return movementSpeed;
        }
        set
        {
            if(value > 400)
            {
                movementSpeed = 400;
            }
            else
            {
                movementSpeed = value;
            }
        }
    }
    public float FieldOfViewAngle
    {
        get
        {
            return fieldOfViewAngle;
        }
        set
        {
            if (value > 360)
            {
                fieldOfViewAngle = 360;
            }
            else
            {
                fieldOfViewAngle = value;
            }
        }
    }
    public float VisibleZone
    {
        get
        {
            return visibleZone;
        }
        set
        {
            if (value > 400)
            {
                visibleZone = 400;
            }
            else
            {
                visibleZone = value;
            }
        }
    }
    public float AgressionZone
    {
        get
        {
            return agressionZone;
        }
        set
        {
            if (value > 400)
            {
                agressionZone = 400;
            }
            else
            {
                agressionZone = value;
            }
        }
    }
    public float OccupiedAreaX
    {
        get
        {
            return occupiedAreaX;
        }
        set
        {
            if (value > 4)
            {
                occupiedAreaX = 4;
            }
            else
            {
                occupiedAreaX = value;
            }
        }
    }
    public float OccupiedAreaZ
    {
        get
        {
            return occupiedAreaZ;
        }
        set
        {
            if (value > 4)
            {
                occupiedAreaZ = 4;
            }
            else
            {
                occupiedAreaZ = value;
            }
        }
    }

}
/*
    public Stats(float movementSpeed, float visibleZone, float angleDamp, Vector2 occupiedArea)
    {
        MovementSpeed = movementSpeed;
        VisibleZone = visibleZone;
        AngleDamp = angleDamp;
        OccupiedArea = occupiedArea;
    }
    */
