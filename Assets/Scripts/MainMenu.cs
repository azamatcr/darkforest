﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour {

    public GameObject[] buttons;
    public AudioSource buttonClick;

    private void Awake()
    {
        for (int i = 1; i < buttons.Length; i++)
        {
            buttons[i].SetActive(false);
        }
    }

    public void SetLevel()
    {
        buttons[0].SetActive(false);
        for (int i = 1; i < buttons.Length; i++)
        {
            buttons[i].SetActive(true);
        }
        buttonClick.Play();
    }

   

    public void QuitGame()
    {
        Application.Quit();
        buttonClick.Play();
    }
}
