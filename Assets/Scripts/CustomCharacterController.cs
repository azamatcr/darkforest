﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CustomCharacterController : MonoBehaviour
{
    private RaycastHit raycastHit;
    private float movementSpeed;
    private Rigidbody characterRigidbody;

    private Vector3 movementInput;
    private Vector3 movementVelocity;

    CharacterPositionStates characterPositionStates;
    CharacterMovingStates characterMovingStates;

    private int collisionCount = 0;

    public float obstaclesSlowDownTime = 1;
    public float obstaclesSlowDownVelocity = 2;

    private bool isSlowDowned = false;
    private bool isSpeededUp = true;
    public GameObject wtext, ltext;

    private void Awake()
    {
        wtext.SetActive(false);
        ltext.SetActive(false);
    }
    private void Start()
    {
        
        characterRigidbody = GetComponent<Rigidbody>();
        movementInput = Vector3.forward;
        movementSpeed = LevelStatsInitializer.CharacterStats.MovementSpeed / 50;

    }

    private void Update()
    {
        if (collisionCount == 0)
        {
            characterPositionStates = CharacterPositionStates.ON_AIR;
        }
        if (characterRigidbody.velocity == Vector3.zero)
        {
            characterMovingStates = CharacterMovingStates.IS_STANDING;
        }
    }

    private void FixedUpdate()
    {
        if (characterPositionStates == CharacterPositionStates.ON_GROUND)
        {
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out raycastHit, 100))
            {
                //Rotation
                Vector3 playerToMouse = raycastHit.point - transform.position;
                playerToMouse.y = 0;
                Quaternion newRotation = Quaternion.LookRotation(playerToMouse);
                transform.rotation = Quaternion.Slerp(transform.rotation, newRotation, Time.deltaTime * 5);
                //characterRigidbody.MoveRotation(newRotation);
            }
            //Movement
            Vector3 forwardDirection = new Vector3(transform.forward.x, 0, transform.forward.z);

            if (forwardDirection.magnitude > 1)
            {
                forwardDirection.Normalize();
            }

            movementVelocity = forwardDirection * movementSpeed * Time.deltaTime;
            transform.position = (transform.position + movementVelocity);
            //characterRigidbody.MovePosition(transform.position + movementVelocity);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        collisionCount++;

        if (collision.gameObject.tag == "Ground")
        {
            characterPositionStates = CharacterPositionStates.ON_GROUND;
        }
        if (collision.gameObject.tag == "Obstacle")
        {
            if(isSlowDowned == false)
            {
                movementSpeed /= obstaclesSlowDownVelocity;
                isSlowDowned = true;
                isSpeededUp = false;
            }
        }
        if (collision.gameObject.tag == "Enemy")
        {
            Debug.Log("You lose! :( ");
            AudioSource[] aud = GameObject.Find("Audio").GetComponents<AudioSource>();
            aud[0].Stop();
            aud[1].Stop();
            ltext.SetActive(true);
            EnemyAI.enemyState = EnemyAI.EnemyState.ON_CATCH_PLAYER;
            //Time.timeScale = 0;
            StartCoroutine(Mainmenu());
        }
        if (collision.gameObject.tag == "Finish")
        {

            Debug.Log("You win! :) ");
            AudioSource[] aud = GameObject.Find("Audio").GetComponents<AudioSource>();
            aud[0].Stop();
            aud[1].Stop();
            wtext.SetActive(true);
            //Time.timeScale = 0;
            StartCoroutine(Mainmenu());


        }
    }

    private void OnCollisionExit(Collision collision)
    {
        collisionCount--;

        if (collision.gameObject.tag == "Ground")
        {
            characterPositionStates = CharacterPositionStates.ON_AIR;
        }
        if (collision.gameObject.tag == "Obstacle")
        {
            if(isSlowDowned == true && isSpeededUp == false)
            {
                StartCoroutine(ObstaclesSpeedUp());
                isSpeededUp = true;
            }
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            characterPositionStates = CharacterPositionStates.ON_GROUND;
        }
        if (collision.gameObject.tag == "Obstacle")
        {
            if (isSlowDowned == false)
            {
                movementSpeed /= obstaclesSlowDownVelocity;
                isSlowDowned = true;
                isSpeededUp = false;
            }
        }
    }

    IEnumerator ObstaclesSpeedUp()
    {
        yield return new WaitForSeconds(obstaclesSlowDownTime);
        movementSpeed *= obstaclesSlowDownVelocity;
        isSlowDowned = false;
    }

    IEnumerator Mainmenu()
    {

        yield return new WaitForSeconds(2);
        SceneManager.LoadScene("MainMenu");
    }
}
