﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeGenerator : MonoBehaviour
{

    public Vector3 firstSpawn;
    public Vector3 secondSpawn;
    public Vector3 thirdSpawn;
    public Vector3 fouthSpawn;

    public Vector3 restForestSpawn;

    private int posX = -94;
    private int posZ = -94;

    public GameObject[] trees;
    public GameObject[] treesForRest;
    public GameObject stones;
    public GameObject[] spikes;
    public GameObject enemy;
    public GameObject house;

    public Transform[] houseSpawn;

    // Use this for initialization
    void Awake()
    {
        for (int i = 0; i < 600; i++)
        {
            Vector3 pos = new Vector3(Random.Range(firstSpawn.x + 10, firstSpawn.x - 10), firstSpawn.y, Random.Range(firstSpawn.z + 115, firstSpawn.z - 115));

            int treeRandom = Random.Range(0, 2);

            GameObject childTree = Instantiate(trees[treeRandom], pos, Quaternion.identity);
            childTree.transform.parent = transform;
        }
        for (int i = 0; i < 600; i++)
        {
            Vector3 pos = new Vector3(Random.Range(secondSpawn.x + 115, secondSpawn.x - 115), firstSpawn.y, Random.Range(secondSpawn.z + 10, secondSpawn.z - 10));

            int treeRandom = Random.Range(0, 2);

            GameObject childTree = Instantiate(trees[treeRandom], pos, Quaternion.identity);
            childTree.transform.parent = transform;
        }
        for (int i = 0; i < 600; i++)
        {
            Vector3 pos = new Vector3(Random.Range(thirdSpawn.x + 10, thirdSpawn.x - 10), firstSpawn.y, Random.Range(thirdSpawn.z + 115, thirdSpawn.z - 115));

            int treeRandom = Random.Range(0, 2);

            GameObject childTree = Instantiate(trees[treeRandom], pos, Quaternion.identity);
            childTree.transform.parent = transform;
        }
        for (int i = 0; i < 600; i++)
        {
            Vector3 pos = new Vector3(Random.Range(fouthSpawn.x + 115, fouthSpawn.x - 115), firstSpawn.y, Random.Range(fouthSpawn.z + 10, fouthSpawn.z - 10));

            int treeRandom = Random.Range(0, 2);

            GameObject childTree = Instantiate(trees[treeRandom], pos, Quaternion.identity);
            childTree.transform.parent = transform;
        }
        
        for (int i = 0; i < 50; i++)
        {
            if (posX < 100)
            {

                posX += 4;
                int curPosX = posX;

                for (int j = 0; j < 50; j++)
                {
                    if (posZ < 100)
                    {
                        //int distanceBetweenTrees = Random.Range(2, 10);  

                        posX = Random.Range(curPosX - 4, curPosX + 4);

                        int distanceBetweenTreesTwo = Random.Range(5, 9);
                        posZ += distanceBetweenTreesTwo;

                        Vector3 pos = new Vector3(posX, 0, posZ);

                        int treeRandom = Random.Range(0, 2);

                        GameObject childTree = Instantiate(treesForRest[treeRandom], pos, Quaternion.identity);
                        childTree.transform.parent = transform;
                    }

                }

                posZ = -94;
            }


        }
        posX = -94;
        for (int i = 0; i < 50; i++)
        {
            
            if (posX < 100)
            {

                posX += 9;
                int curPosX = posX;

                for (int j = 0; j < 50; j++)
                {

                    if (posZ < 100)
                    {
                        //int distanceBetweenTrees = Random.Range(2, 10);  

                        posX = Random.Range(curPosX - 9, curPosX + 9);

                        int distanceBetweenTreesTwo = Random.Range(6, 15);
                        posZ += distanceBetweenTreesTwo;

                        Vector3 pos = new Vector3(posX, 0.27f, posZ);

                        int treeRandom = Random.Range(0, 2);

                        GameObject childTree = Instantiate(spikes[treeRandom], pos, Quaternion.identity);
                        childTree.transform.parent = transform;
                    }

                }

                posZ = -94;
            }
        }

        posX = -94;

        for (int i = 0; i < 50; i++)
        {

            if (posX < 100)
            {

                posX += 12;
                int curPosX = posX;

                for (int j = 0; j < 50; j++)
                {

                    if (posZ < 100)
                    {
                        //int distanceBetweenTrees = Random.Range(2, 10);  

                        posX = Random.Range(curPosX - 12, curPosX + 12);

                        int distanceBetweenTreesTwo = Random.Range(6, 30);
                        posZ += distanceBetweenTreesTwo;

                        Vector3 pos = new Vector3(posX, 0, posZ);
                        

                        GameObject childTree = Instantiate(stones, pos, Quaternion.identity);
                        childTree.transform.parent = transform;
                    }

                }

                posZ = -94;
            }
        }
        posX = -80;

        for (int i = 0; i < 50; i++)
        {

            if (posX < 100)
            {

                posX += 15;
                int curPosX = posX;

                for (int j = 0; j < 50; j++)
                {

                    if (posZ < 100)
                    {
                        //int distanceBetweenTrees = Random.Range(2, 10);  

                        posX = Random.Range(curPosX - 15, curPosX + 15);

                        int distanceBetweenTreesTwo = Random.Range(20, 30);
                        posZ += distanceBetweenTreesTwo;

                        Vector3 pos = new Vector3(posX,0f, posZ);                        

                        GameObject childTree = Instantiate(enemy, pos, Quaternion.identity);
                        childTree.transform.parent = transform;
                    }

                }

                posZ = -94;
            }
        }

        int houseSpawnRandom = Random.Range(0, 3);
        Debug.Log(houseSpawnRandom);
        Instantiate(house, houseSpawn[houseSpawnRandom].position, Quaternion.identity);
        //hero.transform.position = heroSpawn.position;
    }
}
