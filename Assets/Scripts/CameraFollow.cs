﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{

    public GameObject character;
    public Vector3 offsetCameraPosition;

    private Vector3 currentCameraPosition;
    private Vector3 destinationCameraPosition;

    private void Start()
    {
        transform.LookAt(character.transform.position);
        currentCameraPosition = transform.position;
        destinationCameraPosition = character.transform.position;
        offsetCameraPosition = currentCameraPosition - destinationCameraPosition;
    }

    private void LateUpdate()
    {
        transform.position = new Vector3(character.transform.position.x + offsetCameraPosition.x, transform.position.y, character.transform.position.z + offsetCameraPosition.z);
	}
}
