﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyCollision : MonoBehaviour {

    private IEnumerator Start()
    {
        yield return new WaitForSeconds(2);
        //GetComponent<Rigidbody>().isKinematic = true;
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag != "Ground" && collision.gameObject.tag != "Player" && collision.gameObject.tag !="Enemy") 
        {
            Destroy(gameObject);
        }
    }
    
    
}
