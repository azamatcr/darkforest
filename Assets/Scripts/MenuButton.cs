﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuButton : MonoBehaviour {

    public AudioSource source;
    public AudioClip hoverSound;

    public void HoverSound()
    {
        source.PlayOneShot(hoverSound);
    }
}
