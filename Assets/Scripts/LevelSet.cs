﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LevelSet : MonoBehaviour
{
    public static int levelId;
    public AudioSource butClick;

    public void Easy()
    {
        levelId = 1;
        SceneManager.LoadScene("goda");
        butClick.Play();
    }
    public void Medium()
    {
        levelId = 2;
        SceneManager.LoadScene("goda");
        butClick.Play();
    }
    public void Hard()
    {
        levelId = 3;
        SceneManager.LoadScene("goda");
        butClick.Play();
    }
}
