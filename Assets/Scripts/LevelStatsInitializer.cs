﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelStatsInitializer : MonoBehaviour {

    public static Stats CharacterStats;
    public static Stats EnemyStats;
    public static Stats ObstaclesStats;
    public static Stats HouseStats;

    

    private void Awake()
    {
        CharacterStats = new Stats(LevelSet.levelId,"Character");
        EnemyStats = new Stats(LevelSet.levelId, "Enemy");
    }

    

}
