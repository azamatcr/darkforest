﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClearSpawnArea : MonoBehaviour {

    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.tag != "Player" && other.gameObject.tag != "Enemy" && other.gameObject.tag != "Ground")
        {
            Destroy(other.gameObject);
        }
    }
}
