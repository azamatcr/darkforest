﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour
{
    private float movementSpeed;
    private float fieldOfViewAngle;
    private float aggresiveZone;
    private Vector3 personalLastSighting;

    private GameObject character;
    private Rigidbody enemyRigidbody;

    private Vector3 movementVelocity;

    public enum EnemyState
    {
        PLAYER_ON_SIGHT,
        PLAYER_FAR_AWAY,
        ON_CATCH_PLAYER
    }

    public static EnemyState enemyState;
    private Vector3 desiredVelocity;

    Quaternion lookRotation;

    Vector3 lerpedTargetDirection;

    float lerpSpeed = 100;
    private bool isRayCasting = false;

    private void Start()
    {
        //GetComponent<Rigidbody>().isKinematic = false;
        fieldOfViewAngle = LevelStatsInitializer.EnemyStats.FieldOfViewAngle;
        character = GameObject.FindGameObjectWithTag("Player");
        enemyRigidbody = GetComponent<Rigidbody>();
        movementSpeed = LevelStatsInitializer.EnemyStats.MovementSpeed / 50;
        
    }

    private void Update()
    {
        //Debug.DrawRay(transform.position, transform.forward * 10, Color.yellow);
        if (Vector3.Distance(transform.position, character.transform.position) > LevelStatsInitializer.EnemyStats.AgressionZone)
        {
            Debug.Log("1");
            enemyState = EnemyState.PLAYER_FAR_AWAY;
        }
        else if (Vector3.Distance(transform.position, character.transform.position) <= LevelStatsInitializer.EnemyStats.VisibleZone)
        {
            enemyState = EnemyState.PLAYER_ON_SIGHT;
            Debug.Log("2");
        }

        if (enemyState == EnemyState.PLAYER_ON_SIGHT)
        {
            Moving();
            Debug.Log("3");
        }
    }

    void Moving()
    {
        Vector3 lookDirection = (character.transform.position - transform.position).normalized;

        RaycastHit hit;
        RaycastHit hit2;
        RaycastHit hit3;

        float shoulderMultiplier = 1f;
        Vector3 mainPos = transform.position;
        mainPos.y = mainPos.y + 1f;
        Vector3 leftRayPos = transform.position - (transform.right*shoulderMultiplier);
        leftRayPos.y = leftRayPos.y + 1f;
        Vector3 rightRayPos = transform.position + (transform.right*shoulderMultiplier);
        rightRayPos.y = rightRayPos.y + 1f;
        

        if (Physics.Raycast(mainPos, transform.forward, out hit, 1))
        {
            if (hit.transform != transform && hit.transform.gameObject.tag != "Player" && hit.transform.gameObject.tag != "Ground" && hit.transform.gameObject.tag != "Respawn")
            {
                Debug.DrawLine(transform.position, hit.point, Color.red);
                transform.position += transform.right*0.1f;
            }
        }

        else if (Physics.Raycast(leftRayPos, transform.forward, out hit2, 1))
        {
            if (hit2.transform != transform && hit2.transform.gameObject.tag != "Player" && hit2.transform.gameObject.tag != "Ground" && hit2.transform.gameObject.tag != "Respawn")
            {
                Debug.DrawLine(leftRayPos, hit2.point, Color.red);
                transform.position += transform.right * 0.1f;
            }
        }
        else if (Physics.Raycast(rightRayPos, transform.forward, out hit3, 1))
        {
            if (hit3.transform != transform && hit3.transform.gameObject.tag != "Player" && hit3.transform.gameObject.tag != "Ground" && hit3.transform.gameObject.tag != "Respawn")
            {
                Debug.DrawLine(rightRayPos, hit3.point, Color.red);
                transform.position += transform.right * 0.1f;
            }
        }
        else
        {
            Debug.DrawRay(transform.position, transform.forward * 5, Color.yellow);
            Debug.DrawRay(leftRayPos, transform.forward * 5, Color.yellow);
            Debug.DrawRay(rightRayPos, transform.forward * 5, Color.yellow);
        }
        //lerpedTargetDirection = Vector3.Lerp(lerpedTargetDirection, lookDirection, Time.deltaTime * lerpSpeed);

        lookRotation = Quaternion.LookRotation(new Vector3 (lookDirection.x, 0, lookDirection.z));

        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f); //turnSpeed
        

        desiredVelocity = transform.forward * movementSpeed;
        desiredVelocity.y = enemyRigidbody.velocity.y;
        movementVelocity = lookDirection * movementSpeed * Time.deltaTime;
        if (enemyState == EnemyState.PLAYER_ON_SIGHT)
        {

            transform.position = (transform.position + movementVelocity);
            //enemyRigidbody.velocity = desiredVelocity;
        }

    }

}
