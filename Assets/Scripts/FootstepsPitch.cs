﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootstepsPitch : MonoBehaviour
{
    private AudioSource audioSource;
    public AudioClip Snare;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.clip = Snare;
        audioSource.Play();
    }
    private void Update()
    {
        
        audioSource.pitch = Mathf.PingPong(0.9f, Random.Range(0.91f, 0.98f)* Time.time) ;
        
    }
}
